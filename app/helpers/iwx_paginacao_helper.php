<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('IWX_paginacao'))
{
	function IWX_paginacao($atual, $total, $raio, $url, $textos=array()) {
		if($total==1){return false;}
		$url.="/";
		$default_textos=array('first'=>'<<','prev'=>'<','next'=>'>','last'=>'>>');
		$textos=array_replace($default_textos, $textos);
		$paginacao=array();

		$minimo = ($atual - $raio);
		if($minimo < 1) {
			$minimo = 1;
		}

		$maximo = ($atual + $raio);
		if($maximo > $total) {
			$maximo = $total;
		}

		//Primeira
		if($atual != 1){
			$paginacao[]="<li>"."<a href='".$url."1'>".$textos['first']."</a>"."</li>";
		}else{
			$paginacao[]="<li class='disabled'>"."<span>".$textos['first']."</span>"."</li>";
		}

		//Anterior
		if($atual>1){
			$paginacao[]="<li>"."<a href='".$url.($atual-1)."'>".$textos['prev']."</a>"."</li>";
		}else{
			$paginacao[]="<li class='disabled'>"."<span>".$textos['prev']."</span>"."</li>";
		}

		if($minimo>1){
			$paginacao[]="<li>"."<span>...</span>"."</li>";
		}

		for($i = $minimo; $i <= $maximo; $i++) {
			if($i == $atual) {
				$paginacao[]="<li class='active'>"."<span>".$i."</span>"."</li>";
			}else{
				$paginacao[]="<li>"."<a href='".$url.$i."'>".$i."</a>"."</li>";
			}
			
		}

		if($maximo<$total){
			$paginacao[]="<li>"."<span>...</span>"."</li>";
		}
		//Proxima
		if($atual<$total){
			$paginacao[]="<li>"."<a href='".$url.($atual+1)."'>".$textos['next']."</a>"."</li>";
		}else{
			$paginacao[]="<li class='disabled'>"."<span>".$textos['next']."</span>"."</li>";
		}

		//Ultima
		if($atual != $total){
			$paginacao[]="<li>"."<a href='".$url.$total."'>".$textos['last']."</a>"."</li>";
		}else{
			$paginacao[]="<li class='disabled'>"."<span>".$textos['last']."</span>"."</li>";
		}



		return $paginacao;
		unset($atual, $total, $raio, $url, $textos,$default_textos,$minimo,$maximo,$paginacao);
	}
}
