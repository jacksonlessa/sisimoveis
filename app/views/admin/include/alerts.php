<?php if(isset($returns)){ ?>
    <?php foreach($returns as $type=>$msgs){?>
	<?php foreach($msgs as $msg){ ?>
        <div class="alert alert-block alert-<?php echo $type?>">
            <button type="button" class="close" data-dismiss="alert" class=''>
            	<i class='icon-remove'></i>
            </button>
			<span><?php echo $msg?></span>
		</div>
	<?php }?>
	<?php }?>	
<?php }?>
