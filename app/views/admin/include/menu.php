				<div class="sidebar" id="sidebar">

					<!--<div class="sidebar-shortcuts" id="sidebar-shortcuts">
						<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
							<button class="btn btn-success">
								<i class="icon-signal"></i>
							</button>

							<button class="btn btn-info">
								<i class="icon-pencil"></i>
							</button>

							<button class="btn btn-warning">
								<i class="icon-group"></i>
							</button>

							<button class="btn btn-danger">
								<i class="icon-cogs"></i>
							</button>
						</div>

						<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
							<span class="btn btn-success"></span>

							<span class="btn btn-info"></span>

							<span class="btn btn-warning"></span>

							<span class="btn btn-danger"></span>
						</div>
					</div>--><!-- #sidebar-shortcuts -->

					<ul class="nav nav-list">
						<li class="active">
							<a href="index.html">
								<i class="icon-dashboard"></i>
								<span class="menu-text"> Inicio </span>
							</a>
						</li>

						<li>
							<?php echo anchor('admin/newsletter','<i class="icon-list-alt"></i><span class="menu-text"> Newsletter </span>');?>							
						</li>

						<li>
							<?php echo anchor('admin/configuracoes','<i class="icon-cogs"></i><span class="menu-text"> Configurações </span>');?>							
						</li>
						<li>
							<?php echo anchor('admin/imoveis','<i class="icon-signal"></i><span class="menu-text"> Imoveis </span>');?>							
						</li>
						<li>
							<?php echo anchor('admin/usuarios','<i class="icon-group"></i><span class="menu-text"> Usuarios </span>');?>							
						</li>
					</ul><!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
					</div>

					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					</script>
				</div>