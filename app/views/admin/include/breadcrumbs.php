					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<?php foreach ($breadcrumbs as $b){?>
								<li <?php echo (isset($b['active'])?'class="active"':'');?>>
									<?php if(isset($b['icon'])){?>
										<i class="<?php echo $b['icon']?>"></i>
									<?php }?>									
									<?php if(isset($b['active'])){?>
										<?php echo $b['local']?>
									<?php }else{?>
										<?php echo anchor($b['url'],$b['local'])?>
									<?php }?>
								</li>
							<?php }?>

						</ul><!-- .breadcrumb -->

						<!-- <div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- #nav-search -->
					</div>