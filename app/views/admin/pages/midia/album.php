<div class="btn-toolbar pull-right" style='margin-bottom: 10px;'>
    	<?php echo anchor("admin/midia/adicionarFotos/".$album['id_album'],"Novo","class='btn btn-primary'")?>
    	<?php echo anchor("admin","Voltar","class='btn'")?>
</div>
<div class="clearfix"></div>

<?php if($album['imagens']){?>
	<table class="table table-hover table-bordered"><thead><tr>
		<th>Nome</th>
		<th>Caminho</th>
		<th width='84px'>Ações</th></tr></thead><tbody>
		<?php foreach($album['imagens'] as $i){ ?>
	 	<tr>
		<td><?php echo $i['nome_midia'] ?></td>
	 	<td><?php echo $i['caminho']  ?></td>

		<td>
			<?php echo anchor("admin/midia/editarFoto/".$i['id_midia'],"<i class=\"icon-pencil bigger-150  icon-only\"></i>",'class="btn btn-primary btn-xs "')?>
			<?php echo anchor("admin/midia/excluirFoto/".$i['id_midia'],"<i class=\"icon-remove bigger-150  icon-only\"></i>",'class="btn btn-primary btn-xs "')?>
		</td></tr><?php } ?></tbody></table>
<?php }else{?>
    <h3> Nada Cadastrado até o momento</h3>
<?php } ?>