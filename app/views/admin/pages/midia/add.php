<div class="btn-toolbar pull-right" style='margin-bottom: 10px;'>
    	<a class="btn btn-success salvar">Salvar</a>
    	<?php echo anchor("admin/midia","Voltar","class='btn'")?>
</div>
<div class="clearfix"></div>

	<?php echo form_open_multipart("admin/midia/adicionarFotos/".$id_album,'id="form"');?>
	<table id='fotos' class="table table-bordered">
	<thead>
	<tr>
		<th>Titulo</th>
		<th>Imagem</th>
		<th>Remover</th>
	</tr>
	</thead>
	<tbody>
	<?php $count=1;?>
	<?php foreach($idiomas as $k=>$idioma){?>
		<?php if($count){ $count=0;?>
			<tr class='1'>
				<td><input type='text' name='foto[1][<?php echo $k?>]'> <span><?php echo $idioma?></span></td>
				<td rowspan="<?php echo count($idiomas);?>"><input type='file' name='imagem1'></td>
				<td rowspan="<?php echo count($idiomas);?>"><a onclick='removeImg(1)'><i class='icon icon-remove'></i></a></td>
			</tr>
		<?php }else{?>
			<tr class='1'>
				<td><input type='text' name='foto[1][<?php echo $k?>]'> <span><?php echo $idioma?></span></td>
			</tr>
		<?php }?>
	<?php } ?>
	</tbody>
	</table>
	
	<a id='maisum' class='btn'>Adicionar</a>
	<?php echo form_close();?>
		
		<fieldset style='margin: 10px 0'>
		 	<?php anchor("admin/midias_casas","Cancelar","class='btn'");?>
		    <a class="btn btn-success salvar">Salvar</a>
		</fieldset>

<script>
	$('#maisum').click(function(){
		if($('#fotos tbody tr').length){
			var lastId = $('#fotos tbody tr').last().attr('class');
		}else{
			var	lastId = 1;
		}
		var tr='';
		var proxId = parseInt(lastId)+1;
		
		<?php $count=1;?>
		<?php foreach($idiomas as $k=>$idioma){?>
			<?php if($count){ $count=0;?>
				tr += "<tr class='"+proxId+"'><td><input type='text' name='foto["+proxId+"]["+<?php echo $k;?>+"]'> <span><?php echo $idioma?></span></td><td rowspan='"+<?php echo count($idiomas);?>+"'><input type='file' name='imagem"+proxId+"'></td><td rowspan='"+<?php echo count($idiomas);?>+"' ><a onclick='removeImg("+proxId+")'><i class='icon icon-remove'></i></a></td></tr>"
			<?php }else{?>
				tr += "<tr class='"+proxId+"'><td><input type='text' name='foto["+proxId+"]["+<?php echo $k;?>+"]'> <span><?php echo $idioma?></span></td></tr>"
			<?php }?>
		<?php } ?>

		$('#fotos tbody').append(tr);
		
	});
	function removeImg(id){
		$('.'+id).remove();
	}
    $('.salvar').click(function(){
        $('#form').submit();
    })
</script>