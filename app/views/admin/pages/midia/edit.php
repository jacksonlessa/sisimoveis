<div class="btn-toolbar pull-right" style='margin-bottom: 10px;'>
    	<?php echo form_button('salvar',"Salvar","class='btn btn-primary salvar'")?>
    	<?php echo anchor("admin","Voltar","class='btn'")?>
</div>

	<?php echo form_open_multipart("admin/midia/editarFoto/".$item['id_midia'],'id="form"');?>
	<table id='fotos' class="table table-bordered">
	<thead>
	<tr>
		<th>Titulo</th>
		<th>Imagem</th>
	</tr>
	</thead>
	<tbody>
	<?php $count=1;?>
	<?php foreach($idiomas as $k=>$idioma){?>
		<?php if($count){ $count=0;?>
			<tr class='1'>
				<td>
					<input type='text' name='item[descricao][<?php echo $k?>][nome_midia]' value='<?php echo $item['descricao'][$k]['nome_midia']; ?>'> 
					<span><?php echo $idioma?></span>
				</td>
				<td rowspan="<?php echo count($idiomas);?>">
		            <?php if ($item['caminho']){?>
		            	<img alt="" width='50' src="<?php echo base_url($item['caminho'])?>" id='img-imagem'>
		            	<a class='btn btn-danger remove-imagem' rel='imagem'><i class='icon-remove'></i> Remover Imagem</a>
		            <?php }?>
		            <!--</label>   -->
		            
		            <?php echo form_upload('imagem');?>
		            <?php echo form_hidden('imagem-img',$item['caminho']);?>
	            
				</td>
			</tr>
		<?php }else{?>
			<tr class='1'>
				<td>
					<input type='text' name='item[descricao][<?php echo $k?>][nome_midia]' value='<?php echo $item['descricao'][$k]['nome_midia']; ?>'> 
					<span><?php echo $idioma?></span>
				</td>
			</tr>
		<?php }?>
	<?php } ?>
	</tbody>
	</table>

	<?php echo form_close();?>


<script>
	$('#maisum').click(function(){
		if($('#fotos tbody tr').length){
			var lastId = $('#fotos tbody tr').last().attr('class');
		}else{
			var	lastId = 1;
		}
		var tr='';
		var proxId = parseInt(lastId)+1;
		
		<?php $count=1;?>
		<?php foreach($idiomas as $k=>$idioma){?>
			<?php if($count){ $count=0;?>
				tr += "<tr class='"+proxId+"'><td><input type='text' name='foto["+proxId+"]["+<?php echo $k;?>+"]'> <span><?php echo $idioma?></span></td><td rowspan='"+<?php echo count($idiomas);?>+"'><input type='file' name='imagem"+proxId+"'></td><td rowspan='"+<?php echo count($idiomas);?>+"' ><a onclick='removeImg("+proxId+")'><i class='icon icon-remove'></i></a></td></tr>"
			<?php }else{?>
				tr += "<tr class='"+proxId+"'><td><input type='text' name='foto["+proxId+"]["+<?php echo $k;?>+"]'> <span><?php echo $idioma?></span></td></tr>"
			<?php }?>
		<?php } ?>

		$('#fotos tbody').append(tr);
		
	});
	function removeImg(id){
		$('.'+id).remove();
	}
    $('.salvar').click(function(){
        $('#form').submit();
    })


    $('.remove-imagem').click(function(){
		var ref =$(this).attr('rel');
		var img = 'img-'+ref;
		var input = ref+'-img';
		
		$('input[name='+input+']').val('');
		$('#'+img).remove();
		$('a[rel='+ref+']').remove();
  	});	
</script>