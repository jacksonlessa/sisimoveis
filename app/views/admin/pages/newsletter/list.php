<div class="btn-toolbar pull-right" style='margin-bottom: 10px;'>
    	<?php echo anchor("admin/newsletter/export/todos","<i class='icon icon-download'></i>Exportar","class='btn btn-primary'")?>
		<?php
		$class='btn btn-primary';
		if($extrair==0){
			$class.=" disabled";
		}
		?>
    	<!--<?php echo anchor("admin/newsletter/export","<i class='icon icon-download'></i>Exportar","class='".$class."'")?>-->
    	<?php echo anchor("admin","Voltar","class='btn'")?>
</div>
<div class="clearfix"></div>

 	<?php if($itens){?>
    <table class='table'>
	    <thead>
	    	<tr>
				<th>Email</th>
				<th>Data</th>
				<th>Extraido</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($itens as $i){ ?>
	 		<tr>
	 			<td><?php echo $i['email'] ?></td>
	 			<td><?php echo date('d/m/Y H:i',strtotime($i['data'])); ?></td>
	 			<td><?php echo $i['extraido'] ?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
    <?php }else{?>
	    <h3> Nada Cadastrado até o momento</h3>
    <?php } ?>

<?php if($paginacao){ ?>
<ul class="pagination">
<?php foreach($paginacao as $p){
	echo $p;
}?>
</ul>
<?php } ?>