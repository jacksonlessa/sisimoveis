<div style="margin-bottom: 10px; width: 100%;">
    <h3 style="float: left;">Grupos</h3>
    <div style="float: right;"><?=$new?> <?=$remove?></div>
</div>


    <table class="table table-striped table-bordered DataTable dataTable" id="table-list">
    <thead>
    <tr>
        <th><input onclick="$('input[name*=\'id_usuario\']').attr('checked', this.checked);" type="checkbox" /></th>
        <th style="width: 5%; text-align: center;">ID</th>
        <th>NOME</th>
        <th>home</th>
        <th style="width: 10%;text-align: center;">OPÇÕES</th>
    </tr>
    </thead>
    <?php if($grupos){ ?>
        <?php foreach($grupos as $g){ ?>
        <tr>
            <td style="width: 3%;"><?=form_checkbox('id_grupo',$g['id_usu_grupo'])?></td>
            <td style="width: 5%; text-align: center;"><?=$g['id_usu_grupo']?></td>
            <td><?=$g['nome_usu_grupo']?></td>
            <td><?=$g['home_usu_grupo']?></td>
            <td style="width: 10%;text-align: center;">
                <?=anchor('usuarios/grupos/edit/'.$g['id_usu_grupo'],'<i class="icon-pencil"></i>');?>
            </td>
        </tr>
        <?php } ?>
    <?php }else{ ?>
        <tr><td colspan="6">Nenhum registro cadastro até o momento</td></tr>
    <?php } ?>
    </table>
    

<script type="text/javascript">
$(document).ready(function(){
    //var arraycheck;
    var arraycheck={};
    var cont = 0;
    $("#excluir").click(function(){
        

            $("input[type=checkbox][name='id_grupo']:checked").each(function(){
                arraycheck[cont] = $(this).val();
                cont++;
            });

            if(cont == 0){  
                    $(".alert alert-error").remove();
                    $(".row").first().prepend('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button> <span>Selecione ao menos um registro para ser excluido.</span></div>');
                    return false;
            }
            
            var flag = confirm("Deseja realmente excluir o(s) registro(s) selecionado(s)? Esta ação é irreversível.");

            if(flag){
                  $.ajax({
                    type: "POST",
                    url: "<?=BASE_URL()?>index.php/usuarios/grupos/delete",
                    async: false,
                    data: arraycheck,                     
                    error:function(xhr, ajaxOptions, thrownError) { alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText); }                  

                }).done(function(){
                    window.location.href="<?=BASE_URL()?>index.php/usuarios/grupos";
                });
            }else{
                return false;
            }


    });
 }) ;  
    

</script>