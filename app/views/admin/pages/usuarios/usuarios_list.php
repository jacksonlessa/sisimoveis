<div class="btn-toolbar">
    <?php echo anchor("admin/usuarios/novo",'Novo',"class='btn btn-primary'");?>
</div>
<br />
<table class="table table-striped table-bordered DataTable dataTable" id="table-list">
    <thead>
	    <tr>
	        <th>Nome</th>
	        <th>Usuário</th>
	        <th>Grupo</th>
	        <th style="width: 10%;text-align: center;">Opções</th>
	    </tr>
    </thead>
    <?php if($usuarios){ ?>
        <?php foreach($usuarios as $u){ ?>
        <?php 
        	$class='';
        	if(!$u['isactive']){
				$class='error';
			}elseif(!$u['emailconfirmado']){
				$class='warning';
			}
        ?>
        <tr class="<?php echo $class;?>">
            <td><?=$u['nome']?></td>
            <td><?=$u['usuario']?></td>
            <td><?=$niveis[$u['perfil_id']]?></td>
            <td style="width: 10%;text-align: center;">
                <?=anchor('admin/usuarios/editar/'.$u['id'],"<i class=\"icon-pencil  bigger-150  icon-only\"></i>",'class="btn btn-primary btn-xs "')?>
                <?=anchor('admin/usuarios/trocar_senha/'.$u['id'],"<i class=\"icon-lock  bigger-150  icon-only\"></i>",'class="btn btn-primary btn-xs "')?>
                <button class='btn btn-primary btn-xs' onclick='excluir_usuario(<?php echo $u['id'];?>)'><i class='icon-remove  bigger-150 icon-only'></i> </button>
            </td>
        </tr>
        <?php } ?>
    <?php }else{ ?>
        <tr><td colspan="6">Nenhum registro cadastro até o momento</td></tr>
    <?php } ?>
</table>

<script type="text/javascript">
	function excluir_usuario(id){
		var confirmar = confirm("Deseja realmente excluir?");
		if(confirmar){
			window.location = "<?php echo base_url('admin/usuarios/delete/')?>/"+id;
		}
	}
</script>