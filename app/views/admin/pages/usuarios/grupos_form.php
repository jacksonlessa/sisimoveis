<?=form_open($action);?>
<div class="span12">
    <div class="widget">
        <div class="widget-content">
            <?=$save?> <?=$cancel?>
        </div>
    </div>
</div>
<div class="span6">
    <div class="widget">
        <div class="widget-header"><i class="icon-bookmark"></i><h3>Dados</h3></div>
        <div class="widget-content">
            
            
                <?=form_label('Grupo')?>
                <?=form_input('nome_usu_grupo',$grupo['nome_usu_grupo'])?>
                <br /><span class="label label-important"><?=form_error('nome_usu'); ?></span>
                
                <?=form_label('Home')?>
                <?=form_dropdown('home_usu_grupo',$homes,$grupo['home_usu_grupo'])?>
                <br /><span class="label label-important"><?=form_error('login_usu'); ?></span>
                
                
        </div>
    </div>
</div>
<div class="span6">
    <div class="widget">
        <div class="widget-header"><i class="icon-lock"></i><h3>Permissoes</h3></div>

        <div class="widget-content">
                <?=form_fieldset('','style="height:200px;overflow:auto;"' )?>
                <?php foreach($metodos as $k=>$v):?>
                <label>
                <?=form_checkbox('permissoes_usu_grupo[]',$k,(in_array($k,$grupo['permissoes_usu_grupo'])?true:false));?>
                <?=$v;?>
                </label>
                <?php endforeach;?>
                <?=form_fieldset_close()?>
                <br /><span class="label label-important"><?=form_error('permissoes_usu_grupo'); ?></span>
        </div>
            
    </div>
</div>
<?=form_close()?>