<?=form_open('admin/usuarios/'.$action,'class="form-horizontal"');?>
<div class="btn-toolbar">
    	<?=$save?> <?=$cancel?>
</div>

	<?php if($action=='novo'){?>
    <div class="col-sm-6">
    <?php }else{?>
    <div class="col-sm-12">
    <?php }?>
        <h3 class='header smaller'><i class="icon-user"></i>Dados</h3>
		<?=form_hidden('id',$usuario['id']);?>
		
		<div class='form-group'>
			<label class='col-sm-3 control-label no-padding-right'>Data de cadastro</label>
			<div class='col-sm-9'>
			<?=form_input('dataregistro',$usuario['dataregistro'],'readonly="true"')?>
			</div>
		</div>
		<div class='form-group'>
			<label class='col-sm-3 control-label no-padding-right'>Nome</label>
			<div class='col-sm-9'>
			<?=form_input('nome',$usuario['nome'])?>
			<br /><span class="label label-important"><?=form_error('nome'); ?></span>
			</div>
		</div>
		<div class='form-group'>
			<label class='col-sm-3 control-label no-padding-right'>Perfil</label>
			<div class='col-sm-9'>
			<?=form_dropdown('perfil_id',$niveis,$usuario['perfil_id'])?>
			<br /><span class="label label-important"><?=form_error('perfil_id'); ?></span>
			</div>
		</div>
		
		<div class='form-group'>
			<label class='col-sm-3 control-label no-padding-right'>Login</label>
			<div class='col-sm-9'>
			<?=form_input('usuario',$usuario['usuario'],($from=='insert'?'':'readonly="readonly"'))?>
			<br /><span class="label label-important"><?=form_error('usuario'); ?></span>
			</div>
		</div>
		
		<div class='form-group'>
			<label class='col-sm-3 control-label no-padding-right'>Email</label>
			<div class='col-sm-9'>
			<?=form_input('email',$usuario['email'])?>
			<br /><span class="label label-important"><?=form_error('email'); ?></span>
			</div>
		</div>						
    </div>
	<?php if($action=='novo'){?>
    <div class="col-sm-6">
        <h3 class='header smaller'><i class="icon-lock"></i>Senha</h3>

        <div class='form-group'>
			<label class='col-sm-3 control-label no-padding-right'>Senha</label>
			<div class='col-sm-9'>
			<?=form_password('senha')?>
			<br /><span class="label label-important"><?=form_error('senha'); ?></span>
			</div>
		</div>
		
		<div class='form-group'>
			<label class='col-sm-3 control-label no-padding-right'>Confirmar Senha</label>
			<div class='col-sm-9'>
			<?=form_password('conf_senha');?>
			</div>
		</div>
    </div>
	<?php }?>	

<?=form_close()?>
