<?=form_open($action ,'class="row"');?>
<div class="span6" style="padding-top: 40px;">
    <div class="panel">
        <div class="panel-heading"><i class="icon-user"></i>Dados</div>
        <div class="panel-content">
            
            
                <?=form_label('Nome')?>
                <?=form_input('nome',$usuario['nome'])?>
                <br /><span class="label label-important"><?=form_error('nome'); ?></span>
                
                <?=form_label('Login')?>
                <?=form_input('usuario',$usuario['usuario'])?>
                <br /><span class="label label-important"><?=form_error('usuario'); ?></span>
                
                 
                <?=form_label('Email')?>
                <?=form_input('email',$usuario['email'])?>
                <br /><span class="label label-important"><?=form_error('email'); ?></span>
                
                
                
                <div class="form-button" style="margin-top: 10px;">
                <?=$save?> <?=$cancel?>
                </div>
        </div>
    </div>
</div>
<div class="span6" style="padding-top: 40px;">
    <div class="panel">
        <div class="panel-heading"><i class="icon-lock"></i>Senha</div>

        <div class="panel-content">
                <?=form_label('Senha')?>
                <?=form_password('senha_usu')?>
                <br /><span class="label label-important"><?=form_error('senha_usu'); ?></span>
                
                <?=form_label('Confirmar Senha')?>
                <?=form_password('conf_senha')?>
                <div class="form-button" style="margin-top: 10px;">
                <?=$save?> <?=$cancel?>
                </div>
            
        </div>
    </div>
</div>
<?=form_close()?>