<div class="btn-toolbar pull-right" style='margin-bottom:10px;'>
	<?php echo form_submit("",$entry_salvar,"class='btn btn-primary salvar'");?>
    <?php echo anchor("admin/",$entry_cancelar,"class='btn btn-danger'");?>
</div>
<div class="clearfix"></div>

<ul class="nav nav-tabs" id="myTab">
  <li class="active"><a href="#tab1"><?php echo $tab_site?></a></li>
  <li><a href="#tab2"><?php echo $tab_endereco?></a></li>
  <!-- <li><a href="#tab3"><?php echo $tab_redes?></a></li> -->
  <!-- <li><a href="#tab4"><?php echo $tab_imagens?></a></li> -->
  <!-- <li><a href="#tab5"><?php echo $tab_idiomas?></a></li>-->
</ul>

<?php echo form_open('admin/configuracoes','class="tab-content" id="form"')?>

	<div class="tab-pane active" id="tab1">
		<div class='form-group'>
		<?php echo form_label($label_site_name,'class=""');?><br/>
		<?php echo form_input('config[site_name]',$config['site_name'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_descricao);?><br/>
		<?php echo form_textarea('config[descricao]',$config['descricao'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_keywords);?><br/>
		<?php echo form_textarea('config[keywords]',$config['keywords'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_robots);?><br/>
		<?php echo form_input('config[robots]',$config['robots']);?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_analitics,'');?><br/>
		<?php echo form_input('config[analitics]',$config['analitics'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_telefone);?><br/>
		<?php echo form_input('config[telefone]',$config['telefone'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_fax);?><br/>
		<?php echo form_input('config[fax]',$config['fax'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_email);?><br/>
		<?php echo form_input('config[email]',$config['email'])?>
		</div>
	</div>
	<div class="tab-pane" id="tab2">
    	<div class='form-group'>
		<?php echo form_label($label_logradouro);?><br/>
		<?php echo form_input('config[logradouro]',$config['logradouro'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_cidade);?><br/>
		<?php echo form_input('config[cidade]',$config['cidade'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_estado);?><br/>
		<?php echo form_input('config[estado]',$config['estado'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_numero);?><br/>
		<?php echo form_input('config[numero]',$config['numero'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_cep);?><br/>
		<?php echo form_input('config[cep]',$config['cep'])?>
		</div>
	</div>
	<div class="tab-pane" id="tab3">
		<div class='form-group'>
		<?php echo form_label($label_facebook);?><br/>
		<?php echo form_input('config[facebook]',$config['facebook'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_twitter);?><br/>
		<?php echo form_input('config[twitter]',$config['twitter'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_instangram);?><br/>
		<?php echo form_input('config[instangram]',$config['instangram'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_gplus);?><br/>
		<?php echo form_input('config[gplus]',$config['gplus'])?>
		</div>
		<div class='form-group'>
		<?php echo form_label($label_likedin);?><br/>
		<?php echo form_input('config[likedin]',$config['likedin'])?>
		</div>
	</div>
	<div class="tab-pane" id="tab4">
	</div>
	<div class="tab-pane" id="tab5">
		<?php foreach ($idiomas as $idioma){?>
		<div class="checkbox">
			<label>
				<?php echo form_checkbox('idioma['.$idioma['id_idioma'].']', $idioma['id_idioma'], $idioma['ativo']);?>
		    	<?php echo $idioma['nome_idioma']?>
		    </label>
		</div>
		<?php }?>
		
	</div>

<?php echo form_close();?>




   
<script type="text/javascript">
     <!--
     jQuery('document').ready(function($){
     	$('#myTab a').click(function (e) {
     	  e.preventDefault();
     	  $(this).tab('show');
     	});

		$('.salvar').click(function(){
			$('#form').submit()
		})

     });
   	 
      //-->
</script>