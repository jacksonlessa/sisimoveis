<div class="col-xs-12 col-sm-8">
	<div class="widget-box">
		<div class="widget-header header-color-green2">
			<h4>Habilitar/Desabilitar Estados e Cidades</h4>
	
			
		</div>

		<div class="widget-body">
			<div class="widget-main">
				<?php echo form_open('admin/cidades/status')?>
				<select name="status">
					<option value='1'>Habilitar</option>
					<option value='0'>Desabilitar</option>
				</select>
				<?php echo form_dropdown('estado',$estados,'','id="estado"');?>
	            <?php echo form_dropdown('cidade',array(''=>'Cidade'),'','id="cidade"');?>
	            
	            
				<input type='submit' class='btn btn-primary' value='Estado' name='s_estado' />
				<input type='submit' class='btn btn-primary' value='Estado e suas cidades' name='s_estado_cidades' />
				<input type='submit' class='btn btn-primary' value='Cidade' name='s_cidade' />
	            <?php echo form_close()?>
			</div>
		</div>
	</div>
</div>
<div class="col-xs-12 col-sm-8">
	<div class="widget-box">
		<div class="widget-header header-color-green2">
			<h4>Novo Estado</h4>
	
			
		</div>

		<div class="widget-body">
			<div class="widget-main">
				<?php echo form_open('admin/cidades/novo_estado')?>
				<?php echo form_dropdown('pais',$paises);?>
	            
	            <input type='text' value='Nome' name='nome' />
	            <input type='text' value='Sigla' name='sigla' />
	            
	            <input type='submit' class='btn btn-primary' value='Salvar' name='salvar' />
	            <?php echo form_close();?>
			</div>
		</div>
	</div>
</div>
<div class="col-xs-12 col-sm-8">
	<div class="widget-box">
		<div class="widget-header header-color-green2">
			<h4>Nova Cidade</h4>
			
			
		</div>

		<div class="widget-body">
			<div class="widget-main">
			<?php echo form_open('admin/cidades/nova_cidade')?>
				<?php echo form_dropdown('estado',$estados);?>
	            <input type='text' value='Nome' name='nome' />
	            
	            <input type='submit' class='btn btn-primary' value='Salvar' name='salvar' />
	            
	        <?php echo form_close();?>    
			</div>
		</div>
	</div>
</div>
<script>
$('#estado').change(function(){
	var id = $(this).val();
		$.ajax({
			url: '<?php echo base_url()?>/admin/ajax/getCidades/'+id+'/true',
			dataType : 'json',
			beforeSend: function(){
 	        	$("#cidade").html('');
 	        },
 	        success: function(r){
 	            $.each(r, function(k , v){
 	        	$("#cidade").append('<option value="'+k+'">'+v+'</option>');
 	    	});
 		}
	});  
}); 
</script>