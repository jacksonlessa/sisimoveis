<?php $this->load->view('admin/include/header'); ?>


		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>
				<?php $this->load->view('admin/include/menu');?>
				<div class="main-content">
				<?php if($breadcrumbs){?>
				<?php $this->load->view('admin/include/breadcrumbs');?>
				<?php }?>

					<div class="page-content">
						<div class="page-header">
							<h1>
								<?php echo $entry_title;?>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<?php $this->load->view('admin/include/alerts');?>
								

								<?php $this->load->view($contentPage)?>


								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->
			
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
				



<?php $this->load->view('admin/include/footer'); ?>

	</body>
</html>