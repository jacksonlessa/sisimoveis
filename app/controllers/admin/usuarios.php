<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends VTR_Controller{
 
    public function __construct(){
        parent::__construct();

        $this->return['title']='Usuarios';
        $this->return['entry_title']='Usuarios';
       
        $this->load->helper('form_helper');
        
        $this->breadcrumbs->setPart(
        		array(
        				'icon'=>'icon-home home-icon',
        				'local'=>'Home',
        				'url'=>base_url('admin')
        		)
        );
        $this->breadcrumbs->setPart(array('local'=>'Usuarios','url'=>base_url('admin/usuarios')));
        
    }
    public function index(){
        $this->listar();
    }
    
    public function listar($filtro=''){
    	$this->breadcrumbs->setPart(
    			array(
    					'active'=>true,
    					'local'=>'Listar',
    			)
    	);
        $this->return['title']='Lista de usuarios';
        $this->getList();
    }

	public function editar($id){
    	$this->breadcrumbs->setPart(
    			array(
    					'active'=>true,
    					'local'=>'Editar',
    			)
    	);
        $this->return['from']="editar/$id"; 
        $this->return['title']='Alterar usuario';
        
        $this->getForm($id);
    }
    public function novo(){
    	$this->breadcrumbs->setPart(
    			array(
    					'active'=>true,
    					'local'=>'Adicionar',
    			)
    	);
        $this->return['from']="novo";
        $this->return['title']='Novo usuario';
        
        $this->getForm();        
    }
    public function delete($id){
        if($id){
           $this->load->model('usuario/usuarios_model'); 
           if($this->usuarios_model->delUsuarios($id)){
                $this->session->set_flashdata('success','Registros excluídos com sucesso');
           }else{
                $this->session->set_flashdata('error','Não foi possível excluir o(s) registro(s) selecionado(s).');
           } 
        }
        redirect('admin/usuarios');
    }
    
    public function trocar_senha($id=''){
		if(!$id){
			$this->auth->getId();
		}else{
			if($this->auth->getPerfil()!=1){
				redirect('admin/usuarios/trocar_senha/','refresh');
			}
		}
    }
    
    /*Usuarios não Admin */
    public function alterar(){
        $this->return['from']="alterar"; 
        $id=$this->auth->getId();
        
        if($this->auth->getPerfil()==1){
            redirect('admin/usuarios/editar/'.$id,'refresh');
        }

        
        $this->return['title']='Alterar usuario';
        $this->return['action']='usuarios/'.$this->return['from'];
        $this->return['save']    = form_submit("",'Salvar',"class='btn btn-primary'");
        $this->return['cancel']  = anchor("home",'Cancelar',"class='btn btn-danger'");  
        
        $this->load->model('usuario/usuarios_model');
        $usuario = $this->usuarios_model->getUsuario($id);
        
        if($_POST){
            //$this->return['usuario']['id']=$this->input->post('id');
            $this->return['usuario']['nome']=$this->input->post('nome');
            $this->return['usuario']['usuario']=$this->input->post('usuario');
            $this->return['usuario']['email']=$this->input->post('email');
            $this->return['usuario']['senha']=md5($this->input->post('senha'));
            $this->return['cadastro']=$this->input->post('dataregistro');
        }elseif($id){
            //$this->return['usuario']['id']=$usuario['id'];
            $this->return['usuario']['nome']=$usuario['nome'];
            $this->return['usuario']['usuario']=$usuario['usuario'];
            $this->return['usuario']['email']=$usuario['email'];
            $this->return['usuario']['senha']="";
            $this->return['cadastro']=implode('/',array_reverse(explode('-',$usuario['dataregistro'])));
            
        }
        
        if($_POST){
            if ($this->valida()) {
                if($this->usuarios_model->editUsuario($this->return['usuario'],$id)){
                    $this->session->set_flashdata('success','Suas informações foram alteradas com sucesso');
                    redirect('admin/home');
                }

            }else{
                $this->header['error']="Ocorreram erros na validação de seu formulario, por favor, verifique os erros apresentados";
            }
        }
        
        $this->output('usuarios/form');
        
    }    
    
    public function getForm($id=''){
        $this->return['action']=$this->return['from'];
        
        $this->return['save']    = form_submit("",'Salvar',"class='btn btn-primary'");
        $this->return['cancel']  = anchor("admin/usuarios",'Cancelar',"class='btn btn-danger'");  
        
        $this->load->model('usuario/usuarios_model');
        $this->return['niveis']=$this->usuarios_model->getGruposDropDown();
        if($id){
            $usuario = $this->usuarios_model->getUsuario($id);
        }
        
        $this->return['input_option']='';
        
        if($_POST){
            $this->return['usuario']['id']=$this->input->post('id');
            $this->return['usuario']['nome']=$this->input->post('nome');
            $this->return['usuario']['perfil_id']=$this->input->post('perfil_id');
            $this->return['usuario']['usuario']=$this->input->post('usuario');
            if($this->input->post('senha')){
            	$this->return['usuario']['senha']=md5($this->input->post('senha'));
            }
            $this->return['usuario']['email']=$this->input->post('email');
            $this->return['usuario']['dataregistro']=$this->input->post('dataregistro');
        }elseif($id){
            $this->return['usuario']['id']=$usuario['id'];
            $this->return['usuario']['nome']=$usuario['nome'];
            $this->return['usuario']['perfil_id']=$usuario['perfil_id'];
            $this->return['usuario']['usuario']=$usuario['usuario'];
            $this->return['usuario']['email']=$usuario['email'];
            $this->return['usuario']['dataregistro']=implode('/',array_reverse(explode('-',$usuario['dataregistro'])));
        }else{
            $this->return['usuario']['id']="";
            $this->return['usuario']['nome']="";
            $this->return['usuario']['perfil_id']="";
            $this->return['usuario']['usuario']="";
            $this->return['usuario']['email']="";
            $this->return['usuario']['senha']="";
            $this->return['usuario']['dataregistro']=date('d/m/Y');
        }
        
        //verificar
        if($this->input->post()){
            if ($this->valida()) {
                if($id){
                    if($this->usuarios_model->editUsuario($this->return['usuario'],$id)){
                        $this->session->set_flashdata('success','Usuario '.$this->return['usuario']['nome'].' alterado com sucesso');
                        redirect('admin/usuarios/listar');
                    }
                }else{
                    if($this->usuarios_model->addUsuario($this->return['usuario'])){
                        $this->session->set_flashdata('success','Usuario Criado com sucesso');
                        redirect('admin/usuarios/listar');
                    }
                }
            }else{
                $this->header['error']="Ocorreram erros na validação de seu formulario, por favor, verifique os erros apresentados";
            }
        }
        $this->output('usuarios/form');
        
    }
    
    public function getList($filtro=''){
        
        
        if(!$filtro){$filtro=array();}
        
        $this->load->model('usuario/usuarios_model');
        $this->return['niveis']=$this->usuarios_model->getGruposDropDown();
        $this->return['usuarios']=$this->usuarios_model->getUsuarios($filtro);
        
        $this->output('usuarios/usuarios_list');
    }
    
    private function valida(){
        $this->load->helper(array('url'));
        $this->load->library('form_validation');
        
        if($this->return['from']=='novo'){
            $login='required|is_unique[usuarios.usuario]';
            $senha='required|min_length[6]|matches[conf_senha]';
            $perfil_id='required|is_natural_no_zero';
            $email='required|valid_email|is_unique[usuarios.email]';
        }elseif($this->return['from']=='alterar'){
            $perfil_id='is_natural_no_zero';
            $login='required';
            $senha='';
            $email='required|valid_email';
        }else{
            $perfil_id='required|is_natural_no_zero';
            $login='required';
            $senha='';
            $email='required|valid_email';
        }
        
        $this->form_validation->set_rules('nome', 'Nome', 'required|trim');
        $this->form_validation->set_rules('perfil_id', 'Perfil', $perfil_id);
        $this->form_validation->set_rules('usuario', 'Login', $login);
        $this->form_validation->set_rules('email', 'Email', $email);
        $this->form_validation->set_rules('senha', 'Senha', $senha);
        
        return $this->form_validation->run();
    }
}

?>
