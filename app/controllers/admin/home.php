<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends VTR_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->breadcrumbs->setPart(
			array(
					'icon'=>'icon-home home-icon',
					'local'=>'Home',
					'url'=>base_url('admin')
			)
		);
		$this->auth->check_permission();
	}
	public function index(){
		$this->dashboard();
	}
	public function dashboard(){
		$this->breadcrumbs->setPart(
				array(
					'local'=>'Dashboard',
					'active'=>true
				)
			);
		$this->return['entry_title']='Inicio';
		$this->output('home');
	}

}
