<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Midia extends VTR_Controller {

    function __construct() {
        parent::__construct();

		$this->return['idiomas'] = $this->idioma_model->getIdiomaActive();
	}

	public function index(){
		redirect('admin/midia/listar');
	}
	public function listar(){
		$this->load->model('midia/album_model');

		$album=$this->session->userdata('album_tipo');
		$album = explode('/',$album);
		$tipo = $album[0];
		if(isset($album[1])){
			$pai=$album[1];
		}else{
			$pai='';
		}

		$this->return['album']=$this->album_model->tryGet($tipo,$pai);

		$this->output('midia/album');
	}
	public function adicionarFotos($id){
		$this->load->helper('form_helper');
		$this->return['id_album'] = $id;


		$album=$this->session->userdata('album_tipo');
		$album = explode('/',$album);
		$tipo = $album[0];
		if(isset($album[1])){
			$pai=$album[1];
		}else{
			$pai='';
		}

		if($_POST){
			$this->load->model('midia/midia_model');
			$fotos = $this->input->post('foto');
// 			var_dump($fotos);die;
			foreach($fotos as $k=>$foto){
				if($_FILES['imagem'.$k]['name']){
					$img=$this->do_upload('imagem'.$k);
					$save=array(
						'caminho'=>$img,
						'id_album'=>$id,
					);
					$id_midia = $this->midia_model->add($save);
					foreach ($foto as $k=>$nome){
						$desc = array(
							'midia_id_midia'=>$id_midia,
							'idioma_id_idioma'=>$k,
							'nome_midia'=>$nome
						);
						$this->midia_model->addDescricao($desc);
					}

				}

			}
			//$this->midias_casas_model->save();
			$this->setMsg('s',$this->lang->line('alerts_midia_salva'));
			redirect('admin/midia');
		}

		$this->output('midia/add');
	}
	public function editarFoto($id){
		$this->load->helper('form_helper');
		$this->load->model('midia/midia_model');
		$this->return['item'] = $this->midia_model->getMidiaForEdit($id);

		if($_POST){
			$this->return['item']['caminho']=$this->do_upload('imagem');
			
			foreach ($_POST['item']['descricao'] as $k=>$v) {
				$this->return['item']['descricao'][$k]['nome_midia']=$v['nome_midia'];
			}
			
			$this->midia_model->editMidia($this->return['item']);
			$this->setMsg('s',$this->lang->line('alerts_midia_alterada'));
			redirect('admin/midia/listar');
			//echo "<pre>";var_dump($this->return['item']);die;
		}

		$this->output('midia/edit');
	}
	public function excluirFoto($id){
		$this->load->model('midia/midia_model');
		$this->midia_model->excluir($id);
		$this->setMsg('s',$this->lang->line('alerts_midia_excluida'));
		redirect('admin/midia/listar');
	}


	private function do_upload($campo){
		if(!($_FILES[$campo]['name'])){
			return $this->input->post($campo."-img");
		}else{
			$album=$this->session->userdata('album_tipo');
			$album = explode('/',$album);
			$tipo = $album[0];
			if(isset($album[1])){
				$pai=$album[1];
			}else{
				$pai='';
			}
			$path = 'uploads/';
			if($pai){
				$path .='imoveis/'.$pai.'/';
			}
			$path .= $tipo.'/';

			$this->verify_path($path);
			$config['upload_path'] = './'.$path;
			$config['allowed_types'] = '*';
			$config['max_size']	= 20*1024*1024;
			$config['overwrite'] = FALSE; //Não irá sobre-escrever o arquivo
			$config['encrypt_name'] = FALSE; //Não trocará o nome do arquivo para um HASH
	
			$this->load->library('upload', $config);
			if($_FILES[$campo]['name']){
				//var_dump($_FILES[$campo]);die;
				if ( ! $this->upload->do_upload($campo))
				{
					$error = array('error' => $this->upload->display_errors());
					//return $error;
					var_dump($error);die;
				}
				else
				{
					$data = $this->upload->data();
					return  $path.$data['file_name'];
				}
			}else{
				return '';
			}
		}
	}

}