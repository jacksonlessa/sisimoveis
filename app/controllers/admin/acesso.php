<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Acesso extends VTR_Controller{
    
    public function __construct(){
        parent::__construct();


    }

    public function index(){
        $this->login();
    }


    public function login(){
    	//var_dump($this->session);
    	$this->load->helper('form_helper');
        $this->load->model("usuario/usuarios_model");
        
        $this->return['url']=$this->input->get('url');
        if($_POST){
            if( ($usuario = $this->usuarios_model->getLogin($this->input->post('usuario'),md5($this->input->post('senha')))) ){       
            	               
                $this->session->set_userdata(array(
                    'id_usuario'=>$usuario['id'],
                    'nome_usuario'=>$usuario['nome'],
                    'grupo_usuario'=>$usuario['perfil_id'],
                    'home_grupo'=>$usuario['home_usu_grupo'],
                    'logged_in'=>true
                ));
               

                if($this->return['url']){
                    redirect($this->return['url']); 
                }else{  
                    redirect('home/admin');
                }
            }else{
               $this->return['erro'] = $this->lang->line('error_access');
            }
        }
        $this->output('admin/pages/acesso/login',true);
    }
    
    public function logout(){
        $this->session->sess_destroy();
        redirect("home");
    }
}


?>
