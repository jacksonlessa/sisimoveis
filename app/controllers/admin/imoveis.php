<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Imoveis extends VTR_Controller {
    
    function __construct() {
        parent::__construct();
        $this->breadcrumbs->setPart(
        		array(
        				'icon'=>'icon-home home-icon',
        				'local'=>'Home',
        				'url'=>base_url('admin')
        		)
        );
        $this->breadcrumbs->setPart(array('local'=>'Imoveis','url'=>base_url('admin/imoveis')));
        
	}	
	
	public function index()
	{
		$this->listar();
	}
	
	public function listar(){
		$filtro=array();
		//echo $excluido;
		if(isset($_GET['excluido'])){
			$filtro['excluido']=$_GET['excluido'];			
		}else{
			$filtro['excluido']=false;
		}
		
		$this->return['itens']=$this->imoveis_model->get($filtro);
		
		$this->breadcrumbs->setPart( array('active'=>true,'local'=>'Listar'));
        $this->return['title']='Lista de imoveis';

		$this->output('imoveis/list');

	}
	
	public function adicionar(){
		$this->breadcrumbs->setPart( array('active'=>true,'local'=>'Novo'));
        $this->return['title']='Novo imovel';
		$this->getForm();
		
		if($_POST){
			if($this->valida()){
				$this->save();
			}else{
				$this->setMsgForNow('danger',$this->lang->line('alerts_erro_formulario'));
			}
		}
				
		$this->return['from']='adicionar';
		
		$this->output('imoveis/edit');
	}
	
	public function editar($id){
		$this->breadcrumbs->setPart( array('active'=>true,'local'=>'Editar'));
        $this->return['title']='Editar imovel';
		$this->getForm($id);

		if($_POST){
			if($this->valida()){
				$this->save();
			}else{
				$this->setMsgForNow('danger',$this->lang->line('alerts_erro_formulario'));
			}
		}
		
		$this->return['from']='editar/'.$id;
		
		$this->output('imoveis/edit');
	}
	
	public function getForm($id=''){
		if($_POST){
			$item=$_POST['imovel'];
			
			foreach ($item['descricao'] as $k=>$v){
				if($item['descricao'][$k]['nome']==''){$_POST['imovel']['descricao'][$k]['nome']=$item['descricao'][1]['nome'];}
				if($item['descricao'][$k]['valor']==''){$_POST['imovel']['descricao'][$k]['valor']=$item['descricao'][1]['valor'];}
				if($item['descricao'][$k]['valor_faixa']==''){$_POST['imovel']['descricao'][$k]['valor_faixa']=$item['descricao'][1]['valor_faixa'];}
				if($item['descricao'][$k]['area_total']==''){$_POST['imovel']['descricao'][$k]['area_total']=$item['descricao'][1]['area_total'];}
				if($item['descricao'][$k]['area_util']==''){$_POST['imovel']['descricao'][$k]['area_util']=$item['descricao'][1]['area_util'];}
				if($item['descricao'][$k]['descricao']==''){$_POST['imovel']['descricao'][$k]['descricao']=$item['descricao'][1]['descricao'];}
				if($item['descricao'][$k]['localidade']==''){$_POST['imovel']['descricao'][$k]['localidade']=$item['descricao'][1]['localidade'];}
			}
			$item=$_POST['imovel'];	
			$item['destaque']=$this->do_upload('imagem');
			$item['caracteristicas']=$this->input->post('caracteristicas');
			if(!$item['caracteristicas']){$item['caracteristicas']=array();}
// 			$this->imoveis_model->populate($item);
// 			echo "<pre>";var_dump($this->imoveis_model);die;
		}elseif ($id) {
			$this->imoveis_model->get($id);
			$item = $this->imoveis_model->toArray();
			$item['caracteristicas']=$this->imoveis_model->getCaracteristicas($item['id_imoveis']);
		}else{
			$this->imoveis_model->toObject();
			$item = $this->imoveis_model->toArray();
			$item['caracteristicas']=array();
		}
		
		if($item['cidade']){
			if(!isset($item['estado'])){
				$item['estado']=$this->pec->getEstadoByCidade($item['cidade']);
				$item['estado']=$item['estado']['cod_estados'];
			}
		}
		if(isset($item['estado'])){
			if(!isset($item['pais'])){
				$item['pais']=$this->pec->getPaisByEstado($item['estado']);
				$item['pais']=$item['pais']['cod_paises'];
			}
			$this->return['cidades']=$this->pec->getCidadeByEstado($item['estado']);
		}else{
			$item['estado']=0;
		}
		
		if(isset($item['pais'])){
			$this->return['estados']=$this->pec->getEstadosByPais($item['pais']);
		}else{
			$item['pais']=0;
		}
		
		if(!isset($item['hora'])&&!isset($item['data'])){
			if($item['publicacao']!=''){
				$item['publicacao'];
				$data= explode(' ', $item['publicacao']);
				$hora=$data[1];
				$data=implode('-',array_reverse(explode('-', $data[0])));
				$item['data']=$data;
				$item['hora']=$hora;
				$publicacao =$item['publicacao'];
			}else{
				$item['data']='';
				$item['hora']='';
			}
		}
		if(!isset($item['publicacao'])){
			if(isset($item['hora'])&&isset($item['data'])){
				$item['publicacao']=implode('-',array_reverse(explode('-',$item['data']))).' '.$item['hora'];
			}else{
				$item['publicacao']='';
			}
		}
		
		foreach($this->return['idiomas'] as $k=>$idioma){
			$descricao[$k]=array(
					'id_imoveis_descricao'=>'',
					'nome'=>'',
					'valor'=>0,
					'valor_faixa'=>'',
					'area_total'=>'',
					'area_util'=>'',
					'descricao'=>'',
					'localidade'=>'',
			);
		}
		$item['descricao'] = array_replace($descricao,$item['descricao']/*$this->imoveis_model->getDescription()*/);
		
		
		
		$this->return['item']=$item;
	}

	public function desabilitar($id){
		$this->imoveis_model->changeStatus($id);
		
		$this->setMsg('s',$this->lang->line('alerts_desabilidato'));
		
		redirect('admin/imoveis');
	}
	public function habilitar($id){
		$this->imoveis_model->changeStatus($id,true);
		
		$this->setMsg('s',$this->lang->line('alerts_habilidato'));
		
		redirect('admin/imoveis');
	}

	public function remover($id){
		$this->imoveis_model->changeLixeira($id);

		$this->setMsg('s',$this->lang->line('alerts_habilidato'));
		
		redirect('admin/imoveis');
	}

	public function removerLixeira($id){
		$this->imoveis_model->changeLixeira($id,false);

		$this->setMsg('s',$this->lang->line('alerts_habilidato'));
		
		redirect('admin/imoveis');
	}

	public function galeria($tipo,$id){
		
	}

	public function filtros(){
		$this->load->model(array('imoveis/filtro_model'));

		$this->return['tipo'] = $this->filtro_model->getTipos();

		$this->return['valor']=$this->filtro_model->getValores();
		
		$this->return['caracs']=$this->filtro_model->getCaracteristicas();


		if($_POST){
			//tipos
			$tipos=$this->input->post('tipo');
			$this->filtro_model->removeTipos();
			foreach ($tipos as $k=>$idioma){
				foreach ($idioma as $i=>$v){
					if($v!=''){
						$insertTipo[] = array(
								'idioma_id_idioma'=>$i,
								'nome_tipo'=>$v,
								'tipo_id_tipo'=>$k,
						);
					}
				}
			}
			$this->filtro_model->addTipos($insertTipo);

			//valores
			$valor=$this->input->post('valor');
			$this->filtro_model->removeValores();
			foreach ($valor as $idioma=>$valorIdioma){
				foreach ($valorIdioma as $v){
					if($v['fim']||$v['inicio']){
						if(!$v['fim']){
							$v['fim']='99999999999';
						}
						if(!$v['inicio']){
							$v['inicio']=0;
						}
						//echo $idioma. ' - '.$v['fim'].' - '.$v['inicio']."<br/>";
						$insert[] = array(
							'idioma_id_idioma'=>$idioma,
							'inicio_faixa'=>str_replace('.', '',$v['inicio'] ),
							'final_faixa'=>str_replace('.', '',$v['fim'] ),
						);
					}
				}
			}
			$this->filtro_model->addValores($insert);

			//Caracteristicas
			$caracs=$this->input->post('caracs');
			$this->filtro_model->removeCaracteristicas();
			foreach ($caracs as $k=>$idioma){
				foreach ($idioma as $i=>$v){
					if($v!=''){
						$insertCaracs[] = array(
								'idioma_id_idioma'=>$i,
								'nome_caracteristica'=>$v,
								'caracteristica_id'=>$k,
						);
					}
				}
			}
			$this->filtro_model->addCaracteristicas($insertCaracs);
			
			
			redirect('admin/imoveis/filtros');
		}

		$this->output('imoveis/filtro');
	}
	
	private function valida(){
		
		//return false;

		return $this->form_validation->run();
	}
	
	private function do_upload($campo){
		if(!($_FILES[$campo]['name'])){
			return $this->input->post($campo."-img");
		}else{
			$path = 'uploads/imoveis/';
			$this->verify_path($path);
			$config['upload_path'] = './'.$path;
			$config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG|jpeg|JPEG';
			//$config['max_size']	= '1024*10';
			$config['overwrite'] = true; //irá sobre-escrever o arquivo
			$config['encrypt_name'] = false; //Trocará o nome do arquivo para um HASH
				
			$this->load->library('upload', $config);
			if($_FILES[$campo]['name']){
				if ( ! $this->upload->do_upload($campo))
				{
					$error = array('error' => $this->upload->display_errors());
					//return $error;
					var_dump($error);die;
				}
				else
				{
					$data = $this->upload->data();
					return  $path.$data['file_name'];
				}
			}else{
				return '';
			}
		}
	}
	
	private function save(){
		$this->load->model('imoveis/imoveis_description_model');
		$this->imoveis_model->toObject($this->return['item']);
		
		$this->imoveis_model->setIdImoveis($this->return['item']['id_imoveis']);
		$this->imoveis_model->setUsuarioId($this->session->userdata('id_usuario'));
		
		
// 		var_dump($this->return['item']['publicacao']);die;
		if($this->return['item']['publicacao']==' '){
			$this->imoveis_model->setPublicacao(date('Y-m-d H:i:s'));
		}
		
		$this->imoveis_model->save();
		
		$this->imoveis_model->saveCaracteristicas($this->return['item']['id_imoveis'],$this->return['item']['caracteristicas']);
		foreach ($this->return['item']['descricao'] as $id=>$desc){
			$descricao=array(
					'id_imoveis_descricao'=> $desc['id_imoveis_descricao'],
					'nome'=> $desc['nome'],
					'descricao'=> $desc['descricao'],
					'valor'=> str_replace(array('.',','),array('',''),$desc['valor']),
					'valor_faixa'=> $desc['valor_faixa'],
					'area_util'=> $desc['area_util'],
					'area_total'=> $desc['area_total'],
					'localidade'=> $desc['localidade'],
					'imoveis_id_imoveis'=> $this->imoveis_model->getIdImoveis(),
					'idioma_id_idioma'=> $id,
			);
			$obj=new imoveis_description_model();
			$obj->toObject($descricao);
			$obj->save();
		}
		$this->setMsg('s',$this->lang->line('alerts_salvo'));
		redirect('admin/imoveis');
	}
	
}
