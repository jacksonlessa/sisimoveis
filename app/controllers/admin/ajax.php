<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
	
	public function genUrl(){
		$this->load->model('ferramenta/utils','utils');
		
		
		$str = $this->utils->convertToWeb($_POST['titulo']);

		echo $str;
	}
	
	function getEstados($pais){
			
		$this->load->model('pais_estado_cidade','pec');
		$estados = $this->pec->getEstadosByPais($pais);
		echo json_encode($estados);
	}
	
	function getCidades($estado,$all=false){
		 
		$this->load->model('pais_estado_cidade','pec');
		$cidades = $this->pec->getCidadeByEstado($estado,true,$all);
		echo json_encode($cidades);
	}
}