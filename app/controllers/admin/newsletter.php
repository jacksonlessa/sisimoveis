<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends VTR_Controller {
	
	public function __construct(){
		parent::__construct();
		
		$this->return['title']='Newsletter';
        $this->return['entry_title']='Newsletter';
       
        $this->breadcrumbs->setPart(
        		array(
        				'icon'=>'icon-home home-icon',
        				'local'=>'Home',
        				'url'=>base_url('admin')
        		)
        );
        $this->breadcrumbs->setPart(array('local'=>'Newsletter','url'=>base_url('admin/newsletter')));

	
	}
	
	public function index(){
		$this->listar(1);

	}

	public function listar($page=1){
		$this->breadcrumbs->setPart(array('local'=>'Listar','active'=>true));
		if($page<1){$page=1;}
		
		$limit='20';
		$this->load->model('newsletter/newsletter_model','news');
		$this->return['itens']=$this->news->getList(array(),$page,$limit);
		
		$this->return['extrair']=$this->news->countItens(array('extraido'=>0));
		$total=$this->news->countItens();

		$this->load->helper('IWX_paginacao');
		$total_paginas = $total?ceil($total/$limit):1;
		
		$this->return['paginacao'] = IWX_paginacao($page,$total_paginas,2,base_url('admin/newsletter/listar/'));

		
		$this->output('newsletter/list');
	}

	public function export($ativos='ativos'){
		$this->load->model('newsletter/newsletter_model','news');
		$this->load->library('jjl_toolkit');
		if($ativos=='todos'){
			$emails = $this->news->toExportAll();
		}else{
			$emails = $this->news->toExport();
		}
		$this->jjl_toolkit->csvExport($emails,'emails_'.date('d/m/Y_H:i:s').'.csv');
		//redirect('admin/newsletter');
	}

}