<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuracoes extends VTR_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->return['title']='Configuracoes';
        $this->return['entry_title']='Configuracoes';
       
        $this->breadcrumbs->setPart(
        		array(
        				'icon'=>'icon-home home-icon',
        				'local'=>'Home',
        				'url'=>base_url('admin')
        		)
        );
        $this->breadcrumbs->setPart(array('local'=>'Configuracoes','url'=>base_url('admin/configuracoes')));
		$this->load->helper('form_helper');
	}

	public function index(){
		$this->load->model('config/config_model', 'configuracao');
		$this->load->model('config/idioma_model', 'idioma_model');
		
		if($_POST){
			$this->_process();
		}
		$this->return['config']=$this->getConfig();
		$this->return['idiomas']=$this->idioma_model->getIdioma();
		
		$this->output('configuracao/index');
	}
	private function _process(){
		
		$config=$this->input->post('config');
		$idioma=$this->input->post('idioma');
		$this->configuracao->save($config);
		$this->idioma_model->update($idioma);

		redirect('admin/configuracoes');
	}
	private function getConfig(){
		$config=array('site_name'=>'' ,'descricao'=>'' ,'keywords'=>'' ,'robots'=>'' ,'analitics' =>'',
			'telefone'=>'' ,'fax'=>'' ,'email' =>'','logradouro'=>'' ,'cidade'=>'' ,'estado'=>'' ,
			'numero'=>'' ,'cep'=>'' ,'facebook'=>'' ,'twitter'=>'' ,'instangram'=>'' ,
			'gplus'=>'' ,'likedin'=>'' ,
		);
		
		$config1 = $this->configuracao->getConfig();
		$return = array_merge($config,$config1);
		return $return;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */