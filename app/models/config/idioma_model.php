<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class idioma_model extends CI_Model{
	public function getIdioma($where=array()){
		$query = $this->db->from('idioma')
		->where($where)
		->get()->result_array();
		
		return $query;		
	}
	public function getIdiomaActive(){
		$idiomas=$this->getIdioma(array('ativo'=>1));
		$result=array();
		foreach ($idiomas as $idioma){
			$result[$idioma['id_idioma']]=$idioma['sigla_idioma']; 
		}
		return $result;
	}
	
	public function update($data){
		$idiomas = $this->getIdioma();
		foreach ($idiomas as &$idioma){
			if(in_array($idioma['id_idioma'], $data)){
				$idioma['ativo']=1;
			}else{
				$idioma['ativo']=0;
			}
			
		}
		$this->db->update_batch('idioma',$idiomas,'id_idioma');

	}
	public function getIdiomaId($idioma){
		$query = $this->db->from('idioma')
		->select('id_idioma')
		->where(array('sigla_idioma'=>$idioma))
		->get()->row_array();
		
		return $query['id_idioma'];
	}
	public function getDefault(){
		$idiomas=$this->getIdioma(array('default'=>1));
		$result=array();
		foreach ($idiomas as $idioma){
			$result=$idioma;
		}
		return $result;
	}
}