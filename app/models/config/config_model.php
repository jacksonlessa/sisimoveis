<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class config_model extends CI_Model{
	
	private $config;
	public function __construct(){
		$this->loadConfig();
	}
	
	public function loadConfig($name=''){
		if($name){
			$conf = $this->getConfig(array('nome'=>$name));
		}else{
			$conf = $this->getConfig(array('autoload'=>true));
		}
		if($conf){
			foreach ($conf as $k=>$v){
				$this->config[$k]=$v;
			}
		}elseif($name){
			return 'Não existe configuração com o nome: '.$name;
		}
	}
	
	public function getConfig($where=array()){
		$result = $this->db->from('config')
		->where($where)
		->get()->result_array();
		
		$return = array();
		foreach($result as $r){
			$return[$r['nome']]=$r['value'];
		}
		
		return $return;
	}
	
	public function get(){
		return $this->config;
	}
	
	public function save($data){
		foreach ($data as $k=>$v){
			if($this->getConfig(array('nome'=>$k))){
				$this->update(array('value'=>$v),array('nome'=>$k));
			}else{
				$this->add(array('nome'=>$k,'value'=>$v));
			}
		}
	}
	public function add($data){
		$data['autoload']=1;
		$this->db->insert('config',$data);
	}
	public function update($data,$where){
		$this->db->update('config', $data, $where);
	}
}