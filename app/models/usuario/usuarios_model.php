<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends CI_Model{
    
    private $table      = "usuario";
    private $t_grupo    = "usuarios_grupo"; 
    private $pk         = "id";
    private $fk         = "perfil_id";
    private $pk_grupo   = "id_usu_grupo";
   
    public function getUsuarios($options = array(),$limit = array("0","10")){
        $select = array(
        		'id',
        		'nome',
        		'usuario',
        		'isactive',
        		'perfil_id',
        		'emailconfirmado'	
        );
    	$this->db->select($select);
        
        $this->db->where($options);
           
        $this->db->from($this->table);
        $this->db->limit($limit[1],$limit[0]);
        $query = $this->db->get()->result_array();
        
        return $query;
         
    }
    public function getUsuario($id){
        $this->db->select("*");
        $this->db->where($this->pk,$id);  
        $this->db->from($this->table);
        $query = $this->db->get()->row_array();
        
        return $query;
    }
    
    public function addUsuario($values){
        $values['dataregistro']=implode('-',array_reverse(explode('/',$values['dataregistro'])));
        $this->db->insert($this->table,$values);
        
        return $this->db->affected_rows();
    }
    
    public function editUsuario($values,$id){
    	$values['dataregistro']=implode('-',array_reverse(explode('/',$values['dataregistro'])));
        $this->db->update($this->table,$values,array($this->pk=>$id));
        
        return $this->db->affected_rows();
    }
    
    # Nenhum registro sera excluido mas sim alterado o status que serve de flag para visualizacao ou nao pelo sistema    
    public function delUsuarios($keys = array()){
        if($keys){
            $data = array(
               "status_usu"=>0,
            );

            $this->db->update($this->table, $data, array("id_usu"=>$keys));
            return $this->db->affected_rows() > 0;
        }else{
            return false;
        }
    }


    
    
    public function getUsuariosDropDown($options = array('status_usu'=>1)){}
    public function getGruposDropDown(){
        $grps = $this->db->select('id_usu_grupo as id,nome_usu_grupo as nome')
        ->from($this->t_grupo)->get()->result_array();
        
        $return = array();
        
        foreach($grps as $grp){
            $return[$grp['id']]=$grp['nome'];
        }
        return $return;
        
    }
    
    public function getLogin($login,$senha){
        return $this->db->from($this->table)->join($this->t_grupo,$this->fk.'='.$this->pk_grupo)
        ->where("(usuario = '$login' OR email='$login') AND senha = '$senha' AND isactive = 1")
        ->get()->row_array();
    }

}