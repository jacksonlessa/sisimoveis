<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class grupos_model extends CI_Model{
    
    private $table      = "usuarios_grupo";
    private $pk         = "id_usu_grupo";
   
    public function getGrupos($options = array('status_usu_grupo'=>1),$limit = array("0","10")){

        $this->db->where($options);

        $this->db->from($this->table);
        $this->db->limit($limit[1],$limit[0]);
        $query = $this->db->get()->result_array();
        
        return $query;
         
    }
    public function getGrupo($id){
        $this->db->where($this->pk,$id);  
        $this->db->from($this->table);
        $query = $this->db->get()->row_array();
        
        return $query;
    }
    
    public function addGrupo($dados){
        $permissoes = $dados['permissoes_usu_grupo'];
        unset($dados['permissoes_usu_grupo']);
        $this->db->insert($this->table,$dados);
        
        $id=$this->db->insert_id();;
        
        $this->addPermissoes($permissoes,$id);
        
        return $this->db->affected_rows();
    }
    public function editGrupo($dados,$id){
        $permissoes = $dados['permissoes_usu_grupo'];
        unset($dados['permissoes_usu_grupo']);
        
        $this->db->update($this->table,$dados,array($this->pk=>$id));
        
        $this->addPermissoes($permissoes,$id);
        
        return $this->db->affected_rows();
    }

    public function getPermissoes($id){
        $query = $this->db->select('sysp_id_sys_metodo as id')
        ->from('sys_permissoes')->where('sysp_id_usu_grupo',$id)
        ->get()->result_array();
        
        $perms=array();
        foreach($query as $perm){
            $perms[]=$perm['id'];
            
        }
        return $perms;
    }
    public function addPermissoes($permissoes=array(),$id){
        $this->delPermissoes($id);
        
        if($permissoes){
            $insert=array();
            foreach($permissoes as $perm){
                $insert[]=array(
                    'sysp_id_usu_grupo'=> $id,
                    'sysp_id_sys_metodo'=> $perm
                );
            }
            $this->db->insert_batch('sys_permissoes',$insert);
        }
        
    }
    
    public function delPermissoes($id){
        $this->db->delete('sys_permissoes',array('sysp_id_usu_grupo'=>$id));
    }
    public function delUsuarios($keys = array()){
        if($keys){
            $cont = 0;
            foreach($keys as $k){
                $data[] = array(
                   "id_usu_grupo"=>$k,
                   "status_usu_grupo"=>0,
                   "data_mod_usu"=>date("Y-m-d H:m:s")
                );
            }
            $this->db->update_batch($this->table, $data, 'id_usu');
            return $this->db->affected_rows() > 0;
        }else{
            return false;
        }
    }
}