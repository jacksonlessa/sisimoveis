<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_metodos extends CI_Model{
    
    private $table      = "sys_metodos";
    private $pk         = "id_sys_metodos";
   
    public function getMetodosDropDown($key='id_sys_metodo',$name='apelido'){
        $this->db->select("$key , $name");
        $this->db->from($this->table);
        $this->db->order_by($name);
        $metodos = $this->db->get()->result_array();
        
        $return = array();
        foreach($metodos as $metodo){
            $return[$metodo[$key]]=$metodo[$name];
        }
         
         return $return;
    }
}