<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Album_model extends CI_Model{
	
	public function tryGet($tipo, $pai=''){
		$search['tipo']=$tipo;
		if($pai){
			$search['pai']=$pai;
		}

		$result = $this->db->from('album')->where($search)->get()->row_array();
		if( ! $result){
			$id = $this->create($search);
		}else{
			$id = $result['id_album'];
		}

		return $this->getAlbum($id);
	}

	public function create($data){
		$this->load->model('config/idioma_model');
		$this->db->insert('album',$data);
		$id = $this->db->insert_id();

		$idiomas = $this->idioma_model->getIdiomaActive();

		foreach ($idiomas as $k =>$v){
			$this->db->insert('album_descricao',array('album_id_album'=>$id,'idioma_id_idioma'=>$k));
		}

		return $id;
	}

	public function getAlbum($search){
		$this->load->model('midia/midia_model');
		if(is_numeric($search)){
			$where['id_album']=$search;
		}elseif (is_array($search)) {
			$where = $search;
		}

		$album = $this->db->from('album')->where($where)->get()->row_array();

		$album['imagens']=$this->midia_model->getImagens(array('id_album'=>$album['id_album']),true);

		return $album;
	}

	public function getAlbumImovel($data,$imagem_size=array(),$thumb_size=array()){
		$where['idioma_id_idioma']=$this->session->userdata('idioma_id');
		$where = array_replace($where,$data);
		$this->db->from('album a')
		->join('midia m','a.id_album = m.id_album','inner')
		->join('midia_descricao md','m.id_midia = md.midia_id_midia','inner')
		->where($where);

		$result = $this->db->get()->result_array();
		if($result){
			$this->load->model('ferramenta/cropimage');
			$return = array();
			foreach($result as $r){
				$imagem = ($imagem_size ? $this->cropimage->resize($r['caminho'],$imagem_size[0],$imagem_size[1],$imagem_size[2]) : base_url($r['caminho']) );
				
				$thumb = ($thumb_size ? $this->cropimage->resize($r['caminho'],$thumb_size[0],$thumb_size[1],$thumb_size[2]) : '' );

				$return[]=array(
					'imagem'=>$imagem,
					'thumb'=>$thumb,
					'nome'=>$r['nome_midia'],
				);
			}
			return $return;
		}else{
			return false;
		}
	}
}