<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Midia_model extends CI_Model{
	public function getImagens($search,$prepare=false){
		if($prepare){
			$search['idioma_id_idioma']= $this->session->userdata('idioma_id');
		}
		$result= $this->db->from('midia')
		->join('midia_descricao','id_midia=midia_id_midia','inner')
		->where($search)->get()->result_array();


		return $result;


	}
	public function getImagem($id){

	}


	public function add($data){
		$this->db->insert('midia',$data);

		return $this->db->insert_id();
	}

	public function addDescricao($data){
		$this->db->insert('midia_descricao',$data);

		return $this->db->insert_id();
	}

	public function getMidiaForEdit($id){
		$this->db->from('midia')->where(array('id_midia' => $id));
		$result = $this->db->get()->row_array();
		
		$this->db->flush_cache();

		$descricao = $this->db->from('midia_descricao')
		->where(array('midia_id_midia' => 1))
		->get()
		->result_array();

		$descricoes=array();
		foreach ($descricao as $d){
			$descricoes[$d['idioma_id_idioma']]=array(
				'nome_midia'=>$d['nome_midia'],
				'id_midia_descricao'=>$d['id_midia_descricao'],
			);
		}

		$result['descricao']=$descricoes;

		return $result;
	}

	public function editMidia($item){
		$dados=array('caminho'=>$item['caminho']);
		$this->db->update('midia',$dados,array('id_midia'=>$item['id_midia']));
		foreach ($item['descricao'] as $k => $v){
			$dados=array('nome_midia'=>$v['nome_midia']);
			$this->db->update('midia_descricao',$dados,array('midia_id_midia'=>$item['id_midia'],'idioma_id_idioma'=>$k));
		}
	}

	public function excluir($id){
		$this->db->delete('midia_descricao', array('midia_id_midia' => $id));
		$this->db->delete('midia', array('id_midia' => $id));
	}
}
