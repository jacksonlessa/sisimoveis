<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter_model extends CI_Model {
	public function saveNews($dados){
		$dados['data']=date('Y-m-d H:i:s');
		$dados['extraido']=0;
		if($this->db->get_where('newsletter',array('email'=>$dados['email']))->result()){
			$this->db->where('email', $dados['email']);
			$this->db->update('newsletter', $dados);
			$response= array('success','entry_news_atualizada');
		}else{
			$this->db->insert('newsletter', $dados);
			$response= array('success','entry_news_salva');
		}
		return $response;
	}

	public function toExport(){
		$result = $this->db
		->select('email')
		->from('newsletter')
		->where(array('extraido'=>0))
		->order_by('data','asc')
		->get()->result_array();

		$this->db
		->where(array('extraido'=>0))
		->update('newsletter',array('extraido'=>1));

		return $result;
	}

	public function toExportAll(){
		$result = $this->db
		->select('email')
		->from('newsletter')
		->order_by('data','asc')
		->get()->result_array();

		return $result;
	}

	public function getList($filtro=array(),$page=1,$limit=6,$order=array('extraido'=>'asc','data'=>'desc'),$conf=array()){
		$default = array();
		$filtro = array_replace($default,$filtro);
		$offset=($page==1?0:(($page-1)*$limit));
		$this->db
		->from('newsletter n')
		->where($filtro)
		->limit($limit,$offset);
		foreach ($order as $k=>$v){
		$this->db->order_by($k,$v);
		}
		$result = $this->db->get()->result_array();

		
		return $result;

	}

	public function countItens($filtro=array()){
		$default = array();
		$filtro = array_replace($default,$filtro);
		$result = $this->db->from('newsletter n')
		->where($filtro)
		->count_all_results();

		return $result;
	}
}