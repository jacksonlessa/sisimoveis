<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pais_Estado_Cidade extends CI_Model {
	
	public function getPaises($drop=true){
		$consulta = $this->db->get('paises')->result_array();
	
		if($drop){
			$estados[]='Pais';
			foreach ($consulta as $est){
				$estados[$est['cod_paises']]=$est['nome'];
			}
		}else{
			$estados= $consulta;
		}
		return $estados;
	}
	
	public function getEstados($drop=true,$all=false){
		$consulta = $this->db->get('estados')->result_array();
		
		if($drop){
			$estados[]='Estado';
			foreach ($consulta as $est){
				$estados[$est['cod_estados']]=$est['sigla'];
			}
		}else{
			$estados= $consulta;
		}
		return $estados;
	}
	
	public function getPaisById($id){
		return $this->db->get_where('paises',array('cod_paises'=>$id))->row_array();
	}
	
	public function getEstadosById($id){
		return $this->db->get_where('estados',array('cod_estados'=>$id))->row_array();
	}
	
	public function getCidadeById($id){
		$return = $this->db->get_where('cidades',array("cod_cidades"=>$id))->row_array();
		$return['nome']=ucfirst(mb_strtolower($return['nome'],'UTF-8'));
		return $return;
	}
	
	public function getEstadoByCidade($id){
		$cidade = $this->db->get_where('cidades',array("cod_cidades"=>$id))->row_array();
		return $this->getEstadosById($cidade['estados_cod_estados']);
	}
	public function getPaisByEstado($id){
		$estado = $this->db->get_where('estados',array("cod_estados"=>$id))->row_array();
		return $this->getPaisById($estado['pais_cod']);
	}
	
	public function getEstadosByPais($id,$drop=true){
		$consulta = $this->db
		->where(array("pais_cod"=>$id,'est_status'=>1))
		->order_by('nome','desc')
		->get('estados')
		->result_array();
		$estados = array();
		if($drop){
			$estados[]='Estado';
			foreach ($consulta as $cid){
				$estados[$cid['cod_estados']]=ucfirst(mb_strtolower($cid['nome'],'UTF-8'));
			}
		}else{
			$estados= $consulta;
		}
		return $estados;
	}
	
	public function getCidadeByEstado($id,$drop=true,$all=false){
		$search = array('estados_cod_estados'=>$id);
		if(!$all){
			$search['cid_status']=1;
		}
		
		$consulta = $this->db
		->where($search)
		->order_by('nome')
		->get('cidades')
		->result_array();
		$cidades = array();
		if($drop){
			$cidades[]='Cidade';
			foreach ($consulta as $cid){
				$cidades[$cid['cod_cidades']]=ucfirst(mb_strtolower($cid['nome'],'UTF-8'));
			}
		}else{
			$cidades= $consulta;
		}
		return $cidades;
	}
	
	public function getCidadesUtilizadas($title,$estado=''){
		$sql = "
			SELECT c.cod_cidades, c.nome
			FROM  imoveis i
			INNER JOIN cidades c ON c.cod_cidades = i.cidade
			WHERE 
				publicacao <= '".date('Y-m-d H:i:s')."' AND 
				status = 1 AND 
				excluido = 0 ".
				($estado?"AND estados_cod_estados= $estado ":'')
				."
            GROUP BY i.cidade
		";
		$query = $this->db->query($sql)->result_array();
		$return=array(''=>$title);
		foreach($query as $r){
			$return[$r['cod_cidades']]=ucfirst(mb_strtolower($r['nome'],'UTF-8'));
		}

		return $return;
	}
	public function getEstadosUtilizados($title,$pais=''){
		$sql = "
			SELECT  estados.cod_estados , estados.nome
			FROM  imoveis i
			INNER JOIN cidades c ON c.cod_cidades = i.cidade
			INNER JOIN estados ON estados_cod_estados = cod_estados	
			INNER JOIN paises ON pais_cod = cod_paises	
			WHERE 
				publicacao <= '".date('Y-m-d H:i:s')."' AND 
				status = 1 AND 
				excluido = 0 ".
				($pais?"AND pais_cod= $pais ":'')
				."
			GROUP BY cod_estados
		";
		$query = $this->db->query($sql)->result_array();
		$return=array(''=>$title);
		foreach($query as $r){
			$return[$r['cod_estados']]=ucfirst(mb_strtolower($r['nome'],'UTF-8'));
		}

		return $return;
	}
	public function getPaisesUtilizados($title){
		$sql = "
		SELECT  paises.cod_paises , paises.nome
		FROM  imoveis i
		INNER JOIN cidades c ON c.cod_cidades = i.cidade
		INNER JOIN estados ON estados_cod_estados = cod_estados	
		INNER JOIN paises ON pais_cod = cod_paises	
		WHERE 
			publicacao <= '".date('Y-m-d H:i:s')."' AND 
			status = 1 AND 
			excluido = 0
		GROUP BY cod_paises
		";
		$query = $this->db->query($sql)->result_array();
		$return=array(''=>$title);
		foreach($query as $r){
			$return[$r['cod_paises']]=ucfirst(mb_strtolower($r['nome'],'UTF-8'));
		}

		return $return;
	}

	public function changeStatusEstado($estado,$status){
		$this->db->update('estados',array('est_status'=>$status),array('cod_estados'=>$estado));
	}
	
	public function changeStatusEstadoAndCidades($estado,$status){
		$this->changeStatusEstado($estado,$status);
		$this->db->update('cidades',array('cid_status'=>$status),array('estados_cod_estados'=>$estado));
	}
	
	public function changeStatusCidade($cidade,$status){
		$this->db->update('cidades',array('cid_status'=>$status),array('cod_cidades'=>$cidade));
	}
	
	public function addEstado($estado){
		$this->db->insert('estados',$estado);
	}
	
	public function addCidade($cidade){
		$this->db->insert('cidades',$cidade);
	}
}