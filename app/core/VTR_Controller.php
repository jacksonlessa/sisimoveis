<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VTR_Controller extends CI_Controller {
	private $saida;
	public $tema;
	public $return;
	public $msgs;
	
	
	public function __construct(){
		parent::__construct();
		
		$this->return['returns']=($this->session->flashdata('returns')?$this->session->flashdata('returns'):null);
		// 		$this->return['idiomas_bar']=$this->idiomas();
		if($this->router->fetch_directory() == 'admin/'){
			$this->setAdmin();
		}else{
			$this->setFront();
		}
		
		$this->config->load('system_settings');
		$this->load->library('breadcrumbs');
		
		$this->return['site_name']= $this->config->item('site_name');
	}
		
	public function setAdmin(){
		$this->tema = 'iwxadmin';
		$this->saida = 'admin';
		$this->load->library('auth');
		
		$this->lang->load('admin/default','pt-br'/*$this->idioma->getSessionIdioma()*/);
		$this->lang->load('admin/header','pt-br'/*$this->idioma->getSessionIdioma()*/);
		$this->lang->load('admin/footer','pt-br'/*$this->idioma->getSessionIdioma()*/);
		// $this->getLanguage($this->router->fetch_directory(),$this->router->fetch_class(),'pt-br'/*$this->idioma->getSessionIdioma()*/);
	}
	
	public function setFront(){
		$this->saida = 'front';
		$this->tema = 'sys_imoveis';
		
		$this->lang->load('front/default',$this->idioma->getSessionIdioma());
		$this->lang->load('front/header',$this->idioma->getSessionIdioma());
		$this->lang->load('front/footer',$this->idioma->getSessionIdioma());
		$this->getLanguage('front/',$this->router->fetch_class());
	}
	
	public function getContent($file,$vars){
		return $this->load->view($file,$vars,true);
	}
	
	public function output($file='',$custom=false){
		if($this->saida=='front'){
			$this->load->library('tracker');
			$this->tracker->track();
		}
	
		$this->return['contentPage']= $this->saida.'/pages/'.$file;
		$this->saveMsg();
		//$this->extractLang();
		
		$this->return['breadcrumbs']=$this->breadcrumbs->getBreadcrumbs();
		
		if($custom){
			$this->load->view($file,$this->return);
		}else{
			$this->load->view($this->saida."/index.php",$this->return);
		}
	}
	
	/*LANGUAGE FUNCTIONS*/
	private function extractLang(){
		$this->return=array_merge($this->return,$this->lang->extractLang());
	}
	
	private function getLanguage($path='front/',$class,$idioma=''){
		if(!$idioma){$idioma= $this->idioma->getSessionIdioma();}
		$this->lang->load($path.$class, $idioma);
	}
	
	/*IDIOMAS*/
	private function idiomas(){
		$this->load->model('config/idioma_model');
		$ativos=$this->idioma_model->getIdiomaActive();
		$ativo=$this->idioma->getSessionIdioma();
		$idiomas_bar=array();
		foreach ($ativos as $v){
			$is_ativo = ($v==$ativo?true:false);
			$idiomas_bar[$v]=$is_ativo;			
		}
		return $idiomas_bar;
	}
	
	/*MENSAGENS FUNCTIONS*/
	public function setMsg($tipo='',$msg='Mensagem nao definida'){
		$returns =$this->msgs;
		switch ($tipo){
			case 's':
				$returns['success'][]=$msg;
				break;
			case 'e':
				$returns['danger'][]=$msg;
				break;
			case 'w':
				$returns['warning'][]=$msg;
				break;
			case 'i':
			default:
				$returns['info'][]=$msg;
				break;
		}
		$this->msgs=$returns;
		$this->saveMsg();
	}
	public function setMsgForNow($tipo,$msg){
		$this->return['returns'][$tipo][]=$msg;
	}
	private function saveMsg(){
		$this->session->set_flashdata('returns',$this->msgs);
	}
	
	
	/* Verifica caminho e cria se nao existe*/
	public function verify_path($path){
		$p_array= explode('/', $path);
		$pa = './';
		foreach ($p_array as $p){
			$pa .= $p.'/';
			if (!file_exists($pa)) {
				@mkdir($pa, 0777);
			}
		}
	}
}