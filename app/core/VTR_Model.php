<?php
Class VTR_Model extends CI_Model {
	
	static $tabela='';
	static $pk='';
	 
	public function save($data, $id=''){
		if($id){
			return $this->edit($data,$id);
		}else{
			return $this->add($data);
		}
	}
	
	private function add($data){
		$this->db->insert($this->tabela,$data);
	
		return true;
	}
	
	private function edit($data,$id){
		$this->db->update($this->tabela,$data,array($this->pk=>$id));
		return true;
	}
	
	public function delete($id){
		$this->db->delete($this->tabela, array($this->pk => $id));
	}
	
	public function getById($id){
		$query = $this->db->get_where($this->tabela,array($this->pk=>$id))->row_array();
		return $query;
	}
	
	public function get($filtro =array()){
		$query = $this->db->from($this->tabela)
		->where($filtro)
		//->order_by()
		->get()->result_array();
		return $query;
	}
}