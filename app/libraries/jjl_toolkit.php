<?php

class JJL_Toolkit {
	
	
	
	public function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = true)
	{
		$lmin = 'abcdefghijklmnopqrstuvwxyz';
		$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$num = '1234567890';
		$simb = '!@#$%*-';
		$retorno = '';
		$caracteres = '';
	
		$caracteres .= $lmin;
		if ($maiusculas) $caracteres .= $lmai;
		if ($numeros) $caracteres .= $num;
		if ($simbolos) $caracteres .= $simb;
	
		$len = strlen($caracteres);
		for ($n = 1; $n <= $tamanho; $n++) {
			$rand = mt_rand(1, $len);
			$retorno .= $caracteres[$rand-1];
		}
		return $retorno;
	}
	
	public function csvExport($array, $filename = "export.csv", $delimiter=",") {
	    // open raw memory as file so no temp files needed, you might run out of memory though
	    $f = fopen('php://memory', 'w'); 
	    // loop over the input array
	    foreach ($array as $line) { 
	        // generate csv lines from the inner arrays
	        fputcsv($f, $line, $delimiter); 
	    }
	    // rewrind the "file" with the csv lines
	    fseek($f, 0);
	    // tell the browser it's going to be a csv file
	    header('Content-Type: application/csv');
	    // tell the browser we want to save it instead of displaying it
	    header('Content-Disposition: attachement; filename="'.$filename.'"');
	    // make php send the generated csv lines to the browser
	    fpassthru($f);
	}
}