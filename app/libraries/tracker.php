<?php
/**
 * Classe para ratrear a movimentação do usuário pelo site
 *
 * @author    Jackson Lessa <jacksonj.lessa@gmail.com>
 *
 */
class Tracker{
	private $dados=array();
	
	private $ci;
	private $sess_name = 'track_id';
	public function __construct(){
		$this->dados['data'] = date('Y:m:d H:m:s');
	}
	public function getData($dado){
		if(array_key_exists($dado,$this->dados)){
			return $this->dados[$dado];
		}else{
			echo 'variável '.$dado.' não existe!'; 
		}
	}
	public function track(){
		$this->ci=get_instance();
		$this->ci->load->library('user_agent');
		if(!$this->ci->agent->robot()){
			$this->register();
			
			if($this->ci->agent->is_referral()){
				$this->dados['last_uri']=$_SERVER['HTTP_REFERER'];
			}else{
				$this->dados['last_uri']='';
			}
			
			if ($this->ci->agent->is_browser())
			{
				$agent = $this->ci->agent->browser().' '.$this->ci->agent->version();
			}
			elseif ($this->ci->agent->is_mobile())
			{
				$agent = $this->ci->agent->mobile();
			}
			else
			{
				$agent = 'Unidentified User Agent';
			}
			$this->dados['agent']=$agent;
			$this->dados['platform']=$this->ci->agent->platform();
			$this->dados['ip']=$_SERVER['REMOTE_ADDR'];
			$this->dados['current_uri']=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			
			$this->save();
		}
		
		
	}
	
	private function register(){

		if(!$this->ci->session->userdata($this->sess_name)){
			$this->ci->load->library('IWX_Toolkit');
			$this->ci->session->set_userdata('track_id',$this->ci->iwx_toolkit->geraSenha(20));
		}
		$this->dados['id']=$this->ci->session->userdata($this->sess_name);
		
	}
	
	private function save(){
		$result = $this->getSessionId();
		if (!$result){
			$this->addSession();
			$result=$this->getSessionId();
		}
		if($this->getLastPage($result['id_session'])!=$this->dados['current_uri']){
			$this->addTrack($result['id_session']);
		}
		
	}
	private function getSessionId(){
		$sql = "SELECT id_session FROM tracker_session WHERE track_id ='". $this->dados['id']."'";
		return $this->ci->db->query($sql)->row_array();
	}
	private function addSession(){
		$sql = "INSERT INTO tracker_session(track_id, ip, agent, platform) VALUES ('".$this->dados['id']."','".$this->dados['ip']."','".$this->dados['agent']."','".$this->dados['platform']."')";
		$this->ci->db->query($sql);
	}
	private function addTrack($id){
		$sql = "INSERT INTO tracker_track( last_uri, current_uri, time, track_session_id) VALUES ('".$this->dados['last_uri']."','".$this->dados['current_uri']."','".$this->dados['data']."','".$id."')";
		$this->ci->db->query($sql);
	}
	private function getLastPage($id){
		$sql = "SELECT current_uri FROM tracker_track WHERE track_session_id = $id ORDER BY id_track DESC LIMIT 1";
		$result = $this->ci->db->query($sql)->row_array();
		if($result){
			return $result['current_uri'];
		}else{
			return false;
		}
	}
	
}