<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Breadcrumbs {
	private $crumbs = array();
	
	public function setPart($part){
		if(is_array($part)){
			$this->crumbs[]=$part;
			return true;
		}
		return false;
	}
	
	public function getPart($id){
		if (isset($this->crumbs[$id])){
			return $this->crumbs[$id];
		}else{
			return false;
		}
	}
	
	public function getBreadcrumbs(){
		return $this->crumbs;
	}
}