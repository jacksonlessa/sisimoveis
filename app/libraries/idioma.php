<?php
/**
 * Description of idioma
 *
 * @author Daniel Weise
 */
class Idioma {

    function sessionIdioma($idioma) {   
    	if(!is_array($idioma)){
    		$idioma=array('idioma'=>$idioma);
    	}   
        $CI = & get_instance();
        $CI->session->set_userdata($idioma);
    }


    function getSessionIdioma() {
        $CI = & get_instance();
      	if (!$CI->session->userdata('idioma')){
      		$this->setDefault();
      	}
        return ($CI->session->userdata('idioma'));
    }
    function getSessionIdiomaID() {
    	$CI = & get_instance();
    	if (!$CI->session->userdata('idioma_id')){
    		$this->setDefault();
    	}
    	return ($CI->session->userdata('idioma_id'));
    }
    function setDefault(){
    	$CI = & get_instance();
    	$CI->load->model('config/idioma_model');
    	$default = $CI->idioma_model->getDefault();
    	$set=array(
    		'idioma_id'=>$default['id_idioma'],
    		'idioma'=>$default['sigla_idioma'],
    	);
    	$this->sessionIdioma($set);
    }

}

?>
