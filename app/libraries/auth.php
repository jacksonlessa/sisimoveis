<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth
{
    private $CI; 
    private $development = true; 
      
    public function __construct(){
    	$this->CI = &get_instance();        
    }
   
    function check_permission()
    {
    	/*
    	* Criando uma instânCIa do CodeIgniter para poder acessar
    	* banco de dados, sessionns, models, etc... 
    	*/
    	
        $diretorio = $this->CI->router->fetch_directory();
        $classe= $this->CI->router->class;
        $metodo= $this->CI->router->method;
    	/**
    	* Buscando a classe e metodo da tabela sys_metodos
    	*/
        $result = $this->getMetodoId($classe,$metodo,$diretorio);
        
		// Se este metodo ainda não existir na tabela sera cadastrado
		if(count($result)==0){
			$data = array(
               	'modulo' => $diretorio ,
                'classe' => $classe ,
               	'metodo' => $metodo ,
               	'apelido' => ($diretorio?$diretorio: '').$classe .  '/' . $metodo,
				'privado' => ($diretorio=='' ? 0 : 1)
            );

			$this->CI->db->insert('sys_metodos', $data); 
            
			$result = $this->getMetodoId($classe,$metodo,$diretorio);
		}
		//Se ja existir tras as informacoes de publico ou privado
		else{
			
			if($result->privado==0){
				// Escapa da validacao e mostra o metodo.
				return true;
                //return false;
			}else{
				// Se for privado, verifica o login
                $nome = $this->CI->session->userdata('nome_usuario');
		    	$logged_in = $this->CI->session->userdata('logged_in');
                $id_usuario =  $this->CI->session->userdata('id_usuario');
                $id_grupo = $this->CI->session->userdata('grupo_usuario');
		        
				$id_sys_metodos = $result->id_sys_metodo;
				
				// Se o usuario estiver logado vai verificar se tem permissao na tabela.
				if($nome && $logged_in && $id_grupo && $id_usuario){
		            
					$array = array('id_sys_metodo' => $id_sys_metodos, 'id_usu_grupo' => $id_grupo);
					$this->CI->db->where($array);
					$query2 = $this->CI->db->get('sys_permissoes');		
					$result2 = $query2->result();

					// Se não vier nenhum resultado da consulta, manda para página de 
					// usuario sem permissão.
					if(count($result2)==0){
					   if($this->development){
					       return true;   
					   }else{
					       redirect('admin/home', 'refresh');   
					   }
					}else{
						return true;
					}
                } 
		        // Se não estiver logado, sera redireCIonado para o login.
		        else{
                    redirect('admin/acesso/login?url='.uri_string(current_url()));
		            //redirect(base_url().'home/login', 'refresh');
		        }
			}
		}	
    }
    
    /**
    * Método que busca o metodo na tabela de metodos do sistema
    * É importante mantelo para que o plugin funCIone
    */
    function getMetodoId($classe,$metodo,$modulo=''){
        $array = array('classe' => $classe, 'metodo' => $metodo,'modulo'=>$modulo);
		$this->CI->db->where($array);
		$query = $this->CI->db->get('sys_metodos');		
        return $query->row();
    }
    
    /**
    * Método auxiliar para autenticar entradas em menu.
    * Não faz parte do plugin como um todo.
    */
    /*function check_menu($classe,$metodo){
    	$this->CI =& get_instance();
		$sql = "SELECT SQL_CACHE
				count(sys_permissoes.id) as found
				FROM
				sys_permissoes
				INNER JOIN sys_metodos
				ON sys_metodos.id = sys_permissoes.id_metodo
				WHERE id_usuario = '" . $this->CI->session->userdata('id_usuario') . "'
				AND classe = '" . $classe . "'
				AND metodo = '" . $metodo . "'";
		$query = $this->CI->db->query($sql);		
		$result = $query->result();
		return $result[0]->found;
    }*/
    
    public function getId(){
        return $this->CI->session->userdata('id_usuario');
    }
    
    public function getName(){
        return $this->CI->session->userdata('nome_usuario');
    }
    
    public function getPerfil(){
        return $this->CI->session->userdata('grupo_usuario');
    }
    
    public function isLogged(){
        return $this->CI->session->userdata('logged_in');
    }
}